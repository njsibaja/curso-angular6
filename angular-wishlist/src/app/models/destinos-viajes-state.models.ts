import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';

//ESTADO
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const intializeDestinoViajeState = function () {
    return {
        items: [],
        loading: false,
        favorito: null
    };
};

//ACCIONES
export enum DestinoViajeActionTypes {
    NUEVO_DESTINO = '[Destino Viaje] Nuevo',
    ELEGIDO_FAVORITO = '[Destino Viaje] Favorito'
}

export class NuevoDestinoAction implements Action {
    type = DestinoViajeActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinoViajeActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export type DestinoViajeActions = NuevoDestinoAction | ElegidoFavoritoAction;

//REDUCERS
export function reduceDestinoViaje (
    state: DestinosViajesState,
    action: DestinoViajeActions
): DestinosViajesState {
    switch (action.type) {
        case DestinoViajeActionTypes.NUEVO_DESTINO: {
            return{
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino ]
            };
        }
        case DestinoViajeActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            let fav:DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
    }
    return state;
}

//EFFECTS
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregados$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajeActionTypes.NUEVO_DESTINO),
        map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    
    constructor(private actions$: Actions) { }
}